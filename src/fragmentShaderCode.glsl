#version 400 core

out vec4 fragmentColor; // output

in vec3 normalWorld;
in vec3 vertexPositionWorld;
in vec3 vertColor;

uniform vec3 lightPositionWorld1; // we can just pass in the uniform (bypass vertex shader)
uniform vec3 lightPositionWorld2; // we can just pass in the uniform (bypass vertex shader)
uniform vec3 distantLight1; // we can just pass in the uniform (bypass vertex shader)
uniform vec3 distantLight1Color;
uniform vec3 ambientLight; // same for ambient light (bypass vertex shader)
uniform vec3 eyePositionWorld;

void main()
{
  /*
  Good tutorial for basic lighting:
  http://learnopengl.com/#!Lighting/Basic-Lighting
  http://learnopengl.com/#!Lighting/Multiple-lights
  */

  /*
  N = vertex Normal vector
  L = vertex to Light vector

  N    L
  ^   ^
  |  /
  | /
  |/
  O (vertex)
  */

  vec3 result = vec3(0,0,0);

  // ambient light
  vec3 ambLight = ambientLight * vertColor;
  result += ambLight * 0.3;

  // distant lights
  vec3 distantLightDir = normalize(distantLight1);
  float diffCos = dot(normalWorld, distantLightDir);
  vec3 diffLight = distantLight1Color * diffCos * vertColor;
  result += diffLight * 0.7;

  // point lights
  // normalized vector from vertex to light
  /*vec3 lightVectorWorld1 = normalize(lightPositionWorld1 - vertexPositionWorld);
  vec3 lightVectorWorld2 = normalize(lightPositionWorld2 - vertexPositionWorld);
  float brightness1 = max(dot(lightVectorWorld1, normalWorld), 0.0f);
  float brightness2 = max(dot(lightVectorWorld2, normalWorld), 0.0f);

  vec3 lightColor1 = vec3(0.0f, 0.2f, 0.8f);
  vec3 lightColor2 = vec3(1.0f, 0.5f, 0.0f);
  vec4 diffuseLight1 = vec4(lightColor1 * brightness1 * vertColor, 1.0);
  vec4 diffuseLight2 = vec4(lightColor2 * brightness2 * vertColor, 1.0);
  vec4 diffuseLight = diffuseLight0;// + diffuseLight2;// + diffuseLight2;*/

  // specular (don't forget to negate the light vector)
  /*vec3 reflectedLightVectorWorld = reflect(-lightVectorWorld1, normalWorld);
  vec3 eyeVectorWorld = normalize(eyePositionWorld - vertexPositionWorld);
  float spec = dot(reflectedLightVectorWorld, eyeVectorWorld);
  spec = pow(spec, 256);
  vec4 specularLight = vec4(spec, spec, spec, 1);*/

  // we clamp the diffuse because when surfaces are facing the other way from
  // the light, we get negative values and that subtracts from the ambient light
  //fragmentColor = vec4(vertColor, 1) + diffuseLight;/* * ambientLight*/;// + clamp(specularLight, 0, 1);
  fragmentColor = vec4(result, 1.0);
}
