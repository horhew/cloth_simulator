#include <QMouseEvent>
#include <QGuiApplication>

#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <shapeGenerator.h>
#include <ClothSim1.h>
#include <cloth.h>
#include <ngl/NGLInit.h>
#include <fstream>

//**********************************************************************************
//********************************* NGLScene ***************************************
//**********************************************************************************

//----------------------------NGLScene::NGLScene()----------------------------------

NGLScene::NGLScene()
{
  // re-size the widget to that of the parent (in this case the GLFrame passed in on construction)
  setTitle("ClothSim1");
}

//----------------------------NGLScene::~NGLScene()----------------------------------

NGLScene::~NGLScene()
{
  std::cout<<"Shutting down NGL, removing VAO's and Shaders\n";

  // ideally we should have a function (e.g. NGLScene::shutdown() ) to delete VBOs
  glDeleteBuffers(1, &m_vbo);

  // TOASK: Do I need to delete program or does QOpenGLwindow take care of it?
  glUseProgram(0);
  glDeleteProgram(m_programID);
}

//----------------------------NGLScene::resizeGL()----------------------------------

void NGLScene::resizeGL(int _w, int _h)
{
  glViewport(0,0,_w,_h);
  update();
}

//----------------------------NGLScene::readShaderCode()----------------------------

std::string NGLScene::readShaderCode(const char* _filename)
{
  std::cout<<_filename<<std::endl;
  std::ifstream input(_filename);
  if(!input.good())
  {
    std::cerr<<"File \""<<_filename<<"\" failed to load."<<std::endl;
    // (optional) exits the program
    exit(EXIT_FAILURE);
  }

  // read file (by char) from beginning to end (end-of-range iterator)
  std::string shaderCode = std::string(std::istreambuf_iterator<char>(input),
                                       std::istreambuf_iterator<char>());

  // close file stream and return shader code
  input.close();
  return shaderCode;
}

//----------------------------NGLScene::checkStatus()-------------------------------

void NGLScene::checkStatus(const GLuint& _objID, std::string _objName, bool _isShader)
{
  // Get compile/link status from input object
  GLint status;
  if(_isShader) {
    std::cout<<"Compiling "<<_objName<<std::endl;
    glGetShaderiv(_objID, GL_COMPILE_STATUS, &status);
  }
  else
  {
    std::cout<<"Linking "<<_objName<<std::endl;
    glGetProgramiv(_objID, GL_LINK_STATUS, &status);
  }

  if(status != GL_TRUE)
  {
    // Get length of the error message and allocate a buffer of needed size
    GLint infoLogLength;
    if(_isShader) { glGetShaderiv(_objID, GL_INFO_LOG_LENGTH, &infoLogLength); }
    else          { glGetProgramiv(_objID, GL_INFO_LOG_LENGTH, &infoLogLength); }
    GLchar* buffer = new GLchar[infoLogLength];

    // Get error into our buffer
    GLsizei bufferSize; // int (used by OpenGL to output the 'true' error msg length)
    if(_isShader) { glGetShaderInfoLog(_objID, infoLogLength, &bufferSize, buffer); }
    else          { glGetProgramInfoLog(_objID, infoLogLength, &bufferSize, buffer); }

    // Output info
    std::cout<<buffer<<std::endl;

    // Don't forget to delete[] the allocated buffer
    delete[] buffer;
  }
  else
  {
    if(_isShader) { std::cout<<"Compilation of "<<_objName<<" was successful!"<<std::endl; }
    else { std::cout<<"Linking "<<_objName<<" was successful!"<<std::endl; }
  }
}

//-----------------------NGLScene::createSharedVbo()--------------------------------
void NGLScene::createSharedVbo(const std::vector<ShapeData*>& _shapes)
{
  //todo: add overloaded sharedvbo() taking in 1 shape
  // shared VBO size
  GLsizeiptr vboSize = 0;

  // iterate over shapes to build total buffer size
  for(const auto shape : _shapes)
  {
    // add to shared VBO size
    vboSize += shape->vertexBufferSize() + shape->indexBufferSize();
  }

  // generate shared VBO
  glGenBuffers(1, &m_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, // target
               vboSize, // size
               0, // data (we initialize to null)
               GL_DYNAMIC_DRAW); // data usage hint

  // copy data into the VBO
  // it doesn't matter if we bind the buffers to
  // GL_ARRAY_BUFFER or GL_ELEMENTS_ARRAY_BUFFER binding points.
  // Only glDrawElements() cares aboutGL_ELEMENTS_ARRAY_BUFFER and all we have to do
  // ...is bind the same buffer to both GL_ELEMENTS_ARRAY_BUFFER and GL_ARRAY_BUFFER
  // NOTE: that's being done in createVaos()
  GLsizeiptr currOffset = 0;
  for(const auto shape : _shapes)
  {
    glBufferSubData(GL_ARRAY_BUFFER, // target
                 currOffset, //offset
                 shape->vertexBufferSize(), // size
                 shape->vertices().get()); // data
    currOffset += shape->vertexBufferSize();

    glBufferSubData(GL_ARRAY_BUFFER, // target
                 currOffset, //offset
                 shape->indexBufferSize(), // size
                 shape->indices().get()); // data
    currOffset += shape->indexBufferSize();
  }
}

//----------------------------NGLScene::storeNumIndices()---------------------------
void NGLScene::storeNumIndices(const std::vector<ShapeData*>& _shapes)
{
  for(const ShapeData* shape : _shapes)
  {
    // Store number of indices per shape
    m_numIndices[shape->name()] = shape->numIndices();
  }
}

//----------------------------NGLScene::createVaos()--------------------------------
void NGLScene::createVaos(const std::vector<ShapeData*>& _shapes)
{
  // overall byte offset
  GLuint byteOffset = 0;

  // loop over all the provided shapes
  for(const auto shape : _shapes)
  {
    // generate vertex array
    glGenVertexArrays(1, &m_vaos[shape->name()]);

    // bind array (all buffer bindings will be associated with the active VAO)
    glBindVertexArray(m_vaos[shape->name()]);

    // enable vertex attributes
    glEnableVertexAttribArray(0); // Enable the vertex attribute 0 (position)
    glEnableVertexAttribArray(1); // Enable the vertex attribute 1 (color)
    glEnableVertexAttribArray(2); // Enable the vertex attribute 2 (normals)

    // bind buffer
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo);

    // set up data for vertex attributes
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(byteOffset)); // position
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(byteOffset + sizeof(GLfloat) * 3)); // color
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(byteOffset + sizeof(GLfloat) * 6)); // normals

    // increment byte offset and store it for use in other places
    byteOffset += shape->vertexBufferSize();
    m_indexOffset[shape->name()] = byteOffset;
    byteOffset += shape->indexBufferSize();
  }
}

//----------------------------NGLScene::installShaders()-----------------------------

void NGLScene::installShaders()
{
  // Create program
  m_programID = glCreateProgram();

  // Create VSO (Vertex Shader Object)----------------------------
  m_vso = glCreateShader(GL_VERTEX_SHADER);

  // Convert to GL format
  std::string codeBuf = readShaderCode("src/vertexShaderCode.glsl");
  const char* source = codeBuf.c_str();
  glShaderSource(m_vso, 1, &source, NULL);

  // Compile the shader, test for compilation errors
  glCompileShader(m_vso);
  checkStatus(m_vso, "vertexID");

  // Create FSO (Fragment Shader Object)--------------------------
  m_fso = glCreateShader(GL_FRAGMENT_SHADER);

  // Convert to GL format
  codeBuf = readShaderCode("src/fragmentShaderCode.glsl");
  source = codeBuf.c_str();
  glShaderSource(m_fso, 1, &source, NULL);

  // Compile the shader, test for compilation errors
  glCompileShader(m_fso);
  checkStatus(m_fso, "fragmentID");

  // Attach VSO and FSO to the Program Object---------------------
  glAttachShader(m_programID, m_vso);
  glAttachShader(m_programID, m_fso);

  // Link and use the Program Object
  glLinkProgram(m_programID);
  checkStatus(m_programID, "m_programID", false); // check for linking errors
  glUseProgram(m_programID);

  // Delete shader objects (could also delete after glUseProgram)
  glDeleteShader(m_vso);
  glDeleteShader(m_fso);
}

//----------------------------NGLScene::drawShape()------------------------------------

void NGLScene::drawShape(const std::string _shapeType, // shape type
                         const glm::mat4& _mM, // model matrix
                         const glm::mat4& _vpM, // view*projection matrix
                         const GLenum _drawMethod)
{
  // calculate mvp matrix for the geometry
  glm::mat4 mvpM = _vpM * _mM;

  // send data to the shader
  glBindVertexArray(m_vaos[_shapeType]);
  glUniformMatrix4fv(m_uniformLocations["mvp"], 1, GL_FALSE, &mvpM[0][0]);
  glUniformMatrix4fv(m_uniformLocations["m"], 1, GL_FALSE, &_mM[0][0]);
  glDrawElements(_drawMethod, m_numIndices[_shapeType], GL_UNSIGNED_SHORT, (void*)m_indexOffset[_shapeType]);

  // we're done drawing, so delete VBOs and VAOs
  /*glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glDeleteVertexArrays(1, &m_vaos[_shapeType]);
  glDeleteBuffers(1, &m_vbo);*/
}

//----------------------------NGLScene::initializeGL()-------------------------------

void NGLScene::initializeGL()
{
  // initialize the NGL lib
  ngl::NGLInit::instance();

  // wind
  glm::vec3 wind(1,0.5,1);
  m_windAmp = 5.0f;
  m_windFreqMultiplier = 0.5f;

  // create cloth(s)
  m_shapes["cloth1"] = m_shapeGen.make("cloth1", ShapeGenerator::plane,
                                       glm::vec3(1,1,1), 25, 30, 0.125);
  m_clothObjects["cloth1"] = std::unique_ptr<Cloth>( new Cloth("cloth1", m_shapes["cloth1"], 32, 1) );
  Cloth* c1 = m_clothObjects["cloth1"].get();
  c1->setTime(m_time);
  c1->setTimestep(m_dtime);
  c1->enableGravity();
  c1->addExtForce("wind", wind);
  c1->setWindFreqMultiplier(m_windFreqMultiplier);
  c1->setWindAmp(m_windAmp);
  c1->setFixedSide("left");
  m_fixedLeft = true;

  /*
  m_shapes["cloth2"] = m_shapeGen.make("cloth2", ShapeGenerator::plane,
                                       glm::vec3(1,1,1), 30, 30, 0.125);
  m_clothObjects["cloth2"] = std::unique_ptr<Cloth>( new Cloth("cloth2", m_shapes["cloth2"], 32, 1) );
  Cloth* c2 = m_clothObjects["cloth2"].get();
  c2->setTime(m_time);
  c2->setTimestep(m_dtime);
  c2->enableGravity();
  c2->addExtForce("wind", wind);
  c2->setWindFreqMultiplier(m_windFreqMultiplier);
  c2->setWindAmp(m_windAmp);
  c2->setFixedSide("bottom");

  glm::mat4 cloth_rotate = glm::rotate(glm::radians(90.0f), glm::vec3(1, 0, 0));
  cloth_rotate = glm::rotate(glm::radians(180.0f), glm::vec3(0, 1, 0)) * cloth_rotate;
  m_clothRotationMatrices["cloth2"].push_back(cloth_rotate);

  for(glm::mat4& matrix : m_clothRotationMatrices["cloth2"])
  {
    c2->rotate(matrix);
  }
  */
  // install shaders
  installShaders();

  // get uniform locations from the program
  m_uniformLocations["mvp"] = glGetUniformLocation(m_programID, "modelToProjectionMatrix"); // mat4
  m_uniformLocations["ambientLight"] = glGetUniformLocation(m_programID, "ambientLight"); // vec3
  m_uniformLocations["distantLight1"] = glGetUniformLocation(m_programID, "distantLight1"); // vec3
  m_uniformLocations["distantLight1Color"] = glGetUniformLocation(m_programID, "distantLight1Color"); // vec3
  m_uniformLocations["lightPositionWorld1"] = glGetUniformLocation(m_programID, "lightPositionWorld1"); // vec3
  m_uniformLocations["lightPositionWorld2"] = glGetUniformLocation(m_programID, "lightPositionWorld2"); // vec3
  // vec3 position for specular lighting
  m_uniformLocations["eye"] = glGetUniformLocation(m_programID, "eyePositionWorld"); // vec3
  // model transform for world-space lighting
  m_uniformLocations["m"] = glGetUniformLocation(m_programID, "modelToWorldMatrix"); // mat4

  // set background color
  bgColor(102,140,255);

  // glEnable
  glEnable(GL_DEPTH_TEST); // enable depth testing for drawing
  glEnable(GL_MULTISAMPLE); // enable multisampling for smoother drawing
  glEnable(GL_CULL_FACE); // enable face culling
  //glFrontFace(GL_CW); // set ClockWise as front-facing
  //glCullFace(GL_FRONT); // default is GL_BACK
  //glViewport(0,0,width(),height()); // as re-size is not explicitly called we need to do this.

  // start timers
  m_timer = startTimer(10);
}

//----------------------------NGLScene::paintGL()------------------------------------

void NGLScene::paintGL()
{  
  // clear the screen and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(0,0,width(),height());
  //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  // build matrices
  glm::mat4 vM = m_camera.getWorldToViewMatrix(); // view M
  glm::mat4 pM = glm::perspective(glm::radians(60.0f), (float)width()/height(), 0.1f, 100.0f); // projection M
  glm::mat4 vpM = pM * vM; // view_projection M

  // build ambient light
  glm::vec3 ambientLight(0.53f, 0.80f, 0.98f); // sky blue (as the background)
  glUniform3fv(m_uniformLocations["ambientLight"], 1, &ambientLight[0]);

  // build distant lights
  glm::vec3 distantLight1(0.0f, 0.0f, 10.0f);
  glUniform3fv(m_uniformLocations["distantLight1"], 1, &distantLight1[0]);
  glm::vec3 distantLight1Color(1.0f, 235.0/255, 204.0/255); // white
  glUniform3fv(m_uniformLocations["distantLight1Color"], 1, &distantLight1Color[0]);

  // build point lights
  glm::vec3 lightPositionWorld1(2.0f, 0.0f, 0.0f);
  glUniform3fv(m_uniformLocations["lightPositionWorld1"], 1, &lightPositionWorld1[0]);
  glm::vec3 lightPositionWorld2(-2.0f, 0.0f, 0.0f);
  glUniform3fv(m_uniformLocations["lightPositionWorld2"], 1, &lightPositionWorld2[0]);

  // eye position for specular lighting
  glm::vec3 eyePosition = m_camera.position();
  glUniform3fv(m_uniformLocations["eye"], 1, &eyePosition[0]);

  // send shapes to OpenGL
  std::vector<ShapeData*> shapes; // temp storage vector
  m_shapes["cloth1"] = m_clothObjects["cloth1"]->shape();
  m_shapes["cloth1"].storeTo(shapes);
  /*
  m_shapes["cloth2"] = m_clothObjects["cloth2"]->shape();
  m_shapes["cloth2"].storeTo(shapes);
  */

  // store per shape numIndices
  storeNumIndices(shapes);

  // create shared VBO
  createSharedVbo(shapes);

  // create VAOs
  createVaos(shapes);

  // Matrices
  glm::mat4 cloth1_mM = glm::translate(glm::vec3(0.0f, 0.0f, -3.0f));
  glm::mat4 cloth2_mM = glm::translate(glm::vec3(0.0f, 0.0f, 3.0f));
  //glm::mat4 cloth_rotate = glm::rotate(glm::radians(-90.0f), glm::vec3(1, 0, 0));

  // Draw shapes
  distantLight1Color = glm::vec3(255.0/255, 212.0/255, 128.0/255); // light orange
  glUniform3fv(m_uniformLocations["distantLight1Color"], 1, &distantLight1Color[0]);
  glCullFace(GL_BACK);
  drawShape("cloth1", cloth1_mM, vpM);
  //drawShape("cloth2", cloth2_mM, vpM);

  distantLight1Color = glm::vec3(204.0/255, 221.0/255, 255.0/255); // light blue
  glUniform3fv(m_uniformLocations["distantLight1Color"], 1, &distantLight1Color[0]);
  glCullFace(GL_FRONT);
  drawShape("cloth1", cloth1_mM, vpM);
  //drawShape("cloth2", cloth2_mM, vpM);

  // we're done drawing, so delete VBOs and VAOs
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glDeleteVertexArrays(1, &m_vaos["cloth1"]);
  glDeleteBuffers(1, &m_vbo);
}

//----------------------------NGLScene::setColor()----------------------------------
void NGLScene::bgColor(int _r, int _g, int _b, float _a)
{
  float slice = 1.0f/255;
  glClearColor(_r * slice, _g * slice, _b * slice, _a);
}

//----------------------------NGLScene::resetAllCloths()----------------------------
void NGLScene::resetAllCloths()
{
  for (auto it = m_clothObjects.begin(); it != m_clothObjects.end(); it++)
  {
    it->second->reset();
  }
}

//----------------------------NGLScene::exportObj()---------------------------------
void NGLScene::exportObj(std::string _filename)
{
  std::string status;
  std::string file = _filename;
  std::ofstream output(file.append(".obj"));

  if (output.is_open())
  {
    // write vertices
    for (GLuint i = 0; i < m_shapes["cloth1"].numVertices(); i++)
    {
      output<<"v "<<m_shapes["cloth1"].vertices()[i].position().x<<" "
           <<m_shapes["cloth1"].vertices()[i].position().y<<" "
           <<m_shapes["cloth1"].vertices()[i].position().z<<" "
           <<"1.0"<<std::endl;
    }

    // write uvs
    for (unsigned int k = 0; k < m_shapes["cloth1"].numVertices() * 2; k+=2)
    {
      output<<"vt "
        <<m_shapes["cloth1"].uvs()[k]<<" "
        <<m_shapes["cloth1"].uvs()[1+k]
      <<std::endl;
    }

    // write faces (Quads)
    int numFaces = (m_shapes["cloth1"].numRows()-1) * (m_shapes["cloth1"].numCols()-1);
    for (int i = 0; i < numFaces; i++)
    {
      output<<"f ";
      for(unsigned int j = 0; j < 6; j++)
      {
        if(j < 3 || j == 5)
        {
          output<<m_shapes["cloth1"].indices()[i*6 + j] + 1
                <<"/"
                <<m_shapes["cloth1"].indices()[i*6 + j] + 1
                <<" ";
        }
      }
      output<<std::endl;
    }

    status = "OBJ Export successful.";
  }
  else {
    std::cout<<"Unable to open file.";
    status = "OBJ Export failed.";
  }

  output.close();
}

//----------------------------NGLScene::events--------------------------------------

void NGLScene::timerEvent(QTimerEvent* _event )
{
  if(_event->timerId() == m_timer)
  {
    //m_time++;
    for(auto& cloth : m_clothObjects)
    {
      cloth.second.get()->update(m_time, m_dtime);
    }
    m_time += m_dtime;
    // re-draw GL
    update();
  }
}

//---------------------------------------------------------------------------
void NGLScene::mouseMoveEvent (QMouseEvent * _event)
{
  if(m_clicked)
  {
    // camera pan
    m_camera.mouseUpdate(glm::vec2(_event->x(), _event->y()));

    // update the GLWindow and re-draw
    //update();
  }
}


//---------------------------------------------------------------------------
void NGLScene::mousePressEvent ( QMouseEvent * _event)
{
  if(_event->button() == Qt::LeftButton)
  {
    m_clicked = true;
  }
}

//---------------------------------------------------------------------------
void NGLScene::mouseReleaseEvent ( QMouseEvent * _event )
{
  if(_event->button() == Qt::LeftButton)
  {
    m_clicked = false;
  }
}

//---------------------------------------------------------------------------
void NGLScene::wheelEvent(QWheelEvent *_event)
{

}
//---------------------------------------------------------------------------
void NGLScene::keyPressEvent(QKeyEvent *_event)
{
  // this method is called every time the main window recives a key event.
  // we then switch on the key value and set the camera in the GLWindow
  switch (_event->key())
  {
  // escape key to quit
  case Qt::Key_Escape : QGuiApplication::exit(EXIT_SUCCESS); break;
  case Qt::Key_L : glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
  case Qt::Key_P : glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;

  case Qt::Key_W : m_camera.moveForward(); break;
  case Qt::Key_S : m_camera.moveBackward(); break;
  case Qt::Key_A : m_camera.strafeLeft(); break;
  case Qt::Key_D : m_camera.strafeRight(); break;
  case Qt::Key_R : m_camera.moveUp(); break;
  case Qt::Key_F : m_camera.moveDown(); break;
  case Qt::Key_X : resetAllCloths(); break;
  case Qt::Key_N : std::cout<<m_camera.position().x
                         <<", "<<m_camera.position().y
                         <<", "<<m_camera.position().z
                         <<std::endl;
                   std::cout<<m_camera.viewDirection().x
                         <<", "<<m_camera.viewDirection().y
                         <<", "<<m_camera.viewDirection().z
                         <<std::endl;
                   break;

  case Qt::Key_C : m_camera.reset(); break;

  case Qt::Key_T : glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
  case Qt::Key_G : glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;

  case Qt::Key_M : exportObj("frame1"); break;
  case Qt::Key_K : exportObj("frame2"); break;

  case Qt::Key_1 :
    m_windAmp = 1.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(1.0f); }; break;
  case Qt::Key_2 :
    m_windAmp = 2.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(2.0f); }; break;
  case Qt::Key_3 :
    m_windAmp = 3.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(3.0f); }; break;
  case Qt::Key_4 :
    m_windAmp = 4.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(4.0f); }; break;
  case Qt::Key_5 :
    m_windAmp = 5.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(5.0f); }; break;
  case Qt::Key_6 :
    m_windAmp = 6.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(6.0f); }; break;
  case Qt::Key_7 :
    m_windAmp = 7.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(7.0f); }; break;
  case Qt::Key_8 :
    m_windAmp = 8.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(8.0f); }; break;
  case Qt::Key_9 :
    m_windAmp = 9.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(9.0f); }; break;
  case Qt::Key_0 :
    m_windAmp = 0.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(0.0f); }; break;

  case Qt::Key_Q :
    m_windFreqMultiplier -= 0.1f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindFreqMultiplier(m_windFreqMultiplier); };
    break;
  case Qt::Key_E :
    m_windFreqMultiplier += 0.1f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindFreqMultiplier(m_windFreqMultiplier); };
    break;

  case Qt::Key_Up :
    m_windAmp += 1.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(m_windAmp); }; break;
    break;
  case Qt::Key_Down :
    m_windAmp -= 1.0f;
    for(auto& cloth : m_clothObjects) { cloth.second.get()->setWindAmp(m_windAmp); }; break;
    break;

  case Qt::Key_Insert :
    m_clothObjects["cloth1"].get()->setFixedSide("none");
    m_fixedTop = false;
    m_fixedRight = false;
    m_fixedBottom = false;
    m_fixedLeft = false;
    break;
  case Qt::Key_Home :
    if(!m_fixedTop) {m_clothObjects["cloth1"].get()->setFixedSide("top"); m_fixedTop = true;}
    else {m_clothObjects["cloth1"].get()->setFixedSide("top", false); m_fixedTop = false;}
    break;
  case Qt::Key_PageUp :
    if(!m_fixedRight) {m_clothObjects["cloth1"].get()->setFixedSide("right"); m_fixedRight = true;}
    else {m_clothObjects["cloth1"].get()->setFixedSide("right", false); m_fixedRight = false;}
    break;
  case Qt::Key_PageDown :
    if(!m_fixedBottom) {m_clothObjects["cloth1"].get()->setFixedSide("bottom"); m_fixedBottom = true;}
    else {m_clothObjects["cloth1"].get()->setFixedSide("bottom", false); m_fixedBottom = false;}
    break;
  case Qt::Key_End :
    if(!m_fixedLeft) {m_clothObjects["cloth1"].get()->setFixedSide("left"); m_fixedLeft = true;}
    else {m_clothObjects["cloth1"].get()->setFixedSide("left", false); m_fixedLeft = false;}
    break;

  default : break;
  }

  // finally update the GLWindow and re-draw
  update();
}

//---------------------------------------------------------------------------
void NGLScene::keyReleaseEvent(QKeyEvent* _event)
{

}
