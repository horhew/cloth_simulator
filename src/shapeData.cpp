#include <shapeData.h>

//**********************************************************************************
//********************************* ShapeData **************************************
//**********************************************************************************

//----------------------------------ShapeData::data()----------------------------

ShapeData ShapeData::data()
{
    ShapeData data;

    data.m_type = type();
    data.m_name = name();
    data.m_numVertices = numVertices();
    data.m_numIndices = numIndices();
    data.m_numRows = numRows();
    data.m_numCols = numCols();

    // deep copy the unique ptrs
    // vertices
    data.m_vertices.reset(new Vertex[data.m_numVertices]);
    for(GLuint i = 0; i < m_numVertices; i++)
    {
      data.m_vertices[i].setPosition(m_vertices.get()[i].position());
      data.m_vertices[i].setColor(m_vertices.get()[i].color());
      data.m_vertices[i].setNormal(m_vertices.get()[i].normal());
    }

    // indices
    data.m_indices.reset(new GLushort[data.m_numIndices]);
    for(GLuint i = 0; i < m_numIndices; i++)
    {
      data.m_indices[i] = m_indices[i];
    }

    // uvs
    data.m_uvs.reset(new GLfloat[data.m_numVertices * 2]);
    for(GLuint i = 0; i < m_numVertices * 2; i+=2)
    {
      data.m_uvs[i] = m_uvs[i]; // 0, 2, 4, 6 ...
      data.m_uvs[1+i] = m_uvs[1+i]; // 1, 3, 5, 7 ...
      //std::cout<<data.uvs()[i]<<", "<<data.uvs()[1 + i]<<std::endl;
    }

    return data;
}

//----------------------------------ShapeData::storeTo()----------------------------
void ShapeData::storeTo(std::vector<ShapeData*>& _toVec)
{
  _toVec.push_back(this);
}
