#include "vec3.h"

//-------------------------------operator(+)-------------------------------------------
Vec3 Vec3::operator +(const Vec3& _v) const
{
  return Vec3(x + _v.x,
              y + _v.y,
              z + _v.z);
}

//-------------------------------operator(-)------------------------------------------
Vec3 Vec3::operator -(const Vec3& _v) const
{
  return Vec3(x - _v.x,
              y - _v.y,
              z - _v.z);
}

//-------------------------------operator(+=)-------------------------------------------
void Vec3::operator +=(const Vec3& _v)
{
  x += _v.x;
  y += _v.y;
  z += _v.z;

  m_length = sqrt(x*x + y*y + z*z);
}

//-------------------------------operator(-=)-------------------------------------------
void Vec3::operator -=(const Vec3& _v)
{
  x -= _v.x;
  y -= _v.y;
  z -= _v.z;

  m_length = sqrt(x*x + y*y + z*z);
}

//-------------------------------operator(*)-----------------------------------------
float Vec3::operator *(const Vec3& _v) const
{
  return this->dot(_v);
}

Vec3 Vec3::operator *(float _multiplier) const
{
  Vec3 vec(x * _multiplier, y * _multiplier, z * _multiplier);
  return vec;
}

Vec3 operator *(float _f, const Vec3& _v)
{
  return Vec3(_v.x, _v.y, _v.z) * _f;
}

//-------------------------------operator(/)-----------------------------------------
Vec3 Vec3::operator /(float _divisor) const
{
  return Vec3(x/_divisor, y/_divisor, z/_divisor);
}

//-------------------------------operator(<<)----------------------------------------
std::ostream& operator<<(std::ostream& _output, const Vec3& _v)
{
  return _output<<"<"<<_v.x<<","<<_v.y<<","<<_v.z<<">";
}

//-------------------------------operator(==)----------------------------------------
bool Vec3::operator ==(const Vec3& _v) const
{
  return (compare(x, _v.x) && compare(y, _v.y) && compare(z, _v.z));
}

//-------------------------------operator(!=)----------------------------------------
bool Vec3::operator !=(const Vec3& _v) const
{
  return (!compare(x, _v.x) || !compare(y, _v.y) || !compare(z, _v.z));
}

//-------------------------------normal()-------------------------------------------
Vec3 Vec3::normal() const
{
  return Vec3(x/m_length, y/m_length, z/m_length);
}

//-------------------------------normalize()----------------------------------------
void Vec3::normalize()
{
  x /= m_length;
  y /= m_length;
  z /= m_length;
}

//-------------------------------dot()-------------------------------------------
float Vec3::dot(const Vec3& _v) const
{
  Vec3 vec1(x, y, z);
  Vec3 vec2(_v.x, _v.y, _v.z);

  if(vec1.length() != 1.0f) { vec1.normalize(); }
  if(vec2.length() != 1.0f) { vec2.normalize(); }

  // calculate and return dot product
  return vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
}

//-------------------------------cross()-------------------------------------------
Vec3 Vec3::cross(const Vec3& _v, bool _normalize) const
{
  Vec3 cross(y*_v.z + z*_v.y,
             x*_v.z + z*_v.x,
             x*_v.y + y*_v.x);
  if (_normalize) { cross.normalize(); }
  return cross;
}

//-------------------------------length()-------------------------------------------
float Vec3::length() const
{
  return m_length;
}

//-------------------------------print()-------------------------------------------
void Vec3::print() const
{
  std::cout<<"<"<<x<<","<<y<<","<<z<<">";
}

//-------------------------------compare()-------------------------------------------
bool Vec3::compare(float _a, float _b) const
{
  return (_a - 0.001f < _b) && (_a + 0.001f > _b);
}
