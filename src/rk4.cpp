#include <rk4.h>

//------------------------------------Derivative-------------------------------------
Derivative::~Derivative() {}

//-------------------------------integrate()-----------------------------------------
void RK4::integrate(Particle& _particle, float _t, float _dt)
{
  // will likely do a loop over all springs
  // in a Cloth (which will be passed in the fn instead of a single Spring)

  Derivative k1 = evaluate(_particle, _t);
  Derivative k2 = evaluate(_particle, _t, _dt * 0.5f, k1);
  Derivative k3 = evaluate(_particle, _t, _dt * 0.5f, k2);
  Derivative k4 = evaluate(_particle, _t, _dt, k3);

  glm::vec3 dxdt = 1/6.0f * (k1.m_dx + 2.0f*k2.m_dx + 2.0f*k3.m_dx + k4.m_dx) * _dt;
  glm::vec3 dvdt = 1/6.0f * (k1.m_dv + 2.0f*k2.m_dv + 2.0f*k3.m_dv + k4.m_dv) * _dt;

  glm::vec3 zeroVec;
  compareVec(dxdt, zeroVec);
  compareVec(dvdt, zeroVec);

  _particle.update(dxdt, dvdt);
}

//-------------------------------evaluate()-----------------------------------------
Derivative RK4::evaluate(const Particle& _particle, float _t)
{
  // k1
  Derivative out;
  out.m_dx = _particle.velocity(); // position(t + dt) = velocity(t)
  out.m_dv = _particle.force();    // velocity(t + dt) = acceleration(t)

  return out;
}

Derivative RK4::evaluate(const Particle& _particle, float _t, float _dt,
                         const Derivative& _d)
{
  // advance state using the derivative values that were passed in
  Derivative temp;
  temp.m_dx = _particle.position() + _d.m_dx * _dt; // Euler integration of position
  temp.m_dv = _particle.velocity() + _d.m_dv * _dt; // Euler integration of velocity

  // k2-k4
  Derivative out;
  out.m_dx = temp.m_dv; // position = velocity(t + dt)
  // We know the initial force -kx -bv. The new force would be -kx -b(dv).
  // So because -kx is constant, the new force is the old force...
  // ...plus the difference in the velocity-damping forces: -bv -[-b(dv)] = b(dv) -bv
  // A limitation of this method is that a global damping value is used instead of per-spring
  out.m_dv = _particle.force() + m_globalDamping * (_particle.velocity() - temp.m_dv); // velocity = acceleration(t + dt)

  return out;
}
