#include <particle.h>

//-------------------------------ctors/dtors------------------------------------------
Particle::Particle()
{
  m_position.x = 0; m_position.y = 0; m_position.z = 0;
  m_velocity.x = 0; m_velocity.y = 0; m_velocity.z = 0;
  m_normal = glm::vec3(0,1,0);
}

Particle::Particle(const glm::vec3& _position, const glm::vec3& _velocity)
{
  m_position = _position;
  m_velocity = _velocity;
}

//-------------------------------kinematics-------------------------------------------
void Particle::advanceState(const glm::vec3& _x, const glm::vec3& _v)
{
  m_position += _x;
  m_velocity += _v;
}

void Particle::setState(const glm::vec3& _x, const glm::vec3& _v)
{
  m_position = _x;
  m_velocity = _v;
}

void Particle::update(const glm::vec3& _dxdt, const glm::vec3& _dvdt)
{
  // update particle
  if (!isFixed()) {
    m_position += _dxdt;
    m_velocity += _dvdt;
  }

  // reset the accumulated force for the next timestep
  m_force = glm::vec3(0,0,0);
}

//-------------------------------Particle::normal()-----------------------------------

