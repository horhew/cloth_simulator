#include <shapeGenerator.h>
#include <cstring>
#include <vector>
#include <iostream>
#include <string>
#include <assert.h>

#define NUM_ARRAY_ELEMENTS(a) sizeof(a) / sizeof(*a)

//**********************************************************************************
//******************************** ShapeGenerator **********************************
//**********************************************************************************

//----------------------ShapeGenerator::makePlaneVerts()-----------------------------

ShapeData ShapeGenerator::makePlaneVerts(uint _rows, uint _cols, float _scale, glm::vec3 _color)
{
  ShapeData shape;

  float s = 1.0f/_scale; // scale
  shape.m_numVertices = _rows * _cols;
  float halfRows = _rows / 2.0f;
  float halfCols = _cols / 2.0f;
  shape.m_vertices.reset(new Vertex[shape.m_numVertices]);
  shape.m_uvs.reset(new GLfloat[shape.m_numVertices * 2]);

  for(uint row = 0; row < _rows; row++)
  {
    for(uint col = 0; col < _cols; col++)
    {
      Vertex& thisVert = shape.m_vertices.get()[(int)row * _cols + (int)col];

      glm::vec3 pos((int)col/s - halfCols/s, halfRows/s -(int)row/s, 0); // vertical
      //ngl::Vec3 pos((int)col/s - halfCols/s, 0, (int)row/s - halfRows/s); // horizontal

      thisVert.setPosition(pos);
      thisVert.setNormal(glm::vec3(0, 1, 0));
      thisVert.setColor(_color);

      // uvs
      int uIndex = (row * _cols + int(col)) * 2; // U (0, 2, 4, 6...)
      int vIndex = uIndex + 1;
      shape.m_uvs[uIndex] = col * 1.0f/(_cols - 1);
      shape.m_uvs[vIndex] = 1 - row * 1.0f/(_cols - 1); // V ( 1, 3, 5, 7...)

      //std::cout<<row * _cols + int(col)<<" U: "<<shape.m_uvs[uIndex]<<std::endl;
      //std::cout<<row * _cols + int(col)<<" V: "<<shape.m_uvs[vIndex]<<std::endl;
    }
  }

  return shape;
}

//-------------------------ShapeGenerator::makePlaneIndices()------------------------

ShapeData ShapeGenerator::makePlaneIndices(uint _rows, uint _cols)
{
  ShapeData shape;

  // 2 triangles per square, 3 indices per triangle
  shape.m_numIndices = (_rows - 1) * (_cols - 1) * 2 * 3;
  shape.m_indices.reset(new GLushort[shape.m_numIndices]);

  int runner = 0;
  for(uint row = 0; row < _rows - 1; row++)
  {
    for(uint col = 0; col < _cols - 1; col++)
    {
      shape.m_indices.get()[runner++] = _cols * row + col;
      shape.m_indices.get()[runner++] = _cols * row + col + _cols;
      shape.m_indices.get()[runner++] = _cols * row + col + _cols + 1;

      shape.m_indices.get()[runner++] = _cols * row + col;
      shape.m_indices.get()[runner++] = _cols * row + col + _cols + 1;
      shape.m_indices.get()[runner++] = _cols * row + col + 1;
    }
  }
  assert(runner = shape.m_numIndices);
  return shape;
}

//--------------------------ShapeGenerator::makePlane()------------------------------

ShapeData ShapeGenerator::makePlane(uint _rows, uint _cols, float _scale, glm::vec3 _color)
{
  ShapeData shape = makePlaneVerts(_rows, _cols, _scale, _color);
  ShapeData shapeIndices = makePlaneIndices(_rows, _cols);
  shape.m_numIndices = shapeIndices.m_numIndices;
  shape.m_indices = std::move(shapeIndices.m_indices);

  // type & dimensions
  shape.m_type = "plane";
  shape.m_numRows = _rows;
  shape.m_numCols = _cols;

  return shape;
}

//--------------------------ShapeGenerator::make()-----------------------------------
ShapeData ShapeGenerator::make(std::string _name, ShapeType _type, glm::vec3 _color,
                               uint _rows, uint _cols, float _scale)
{
  ShapeData shape;

  // Create appropriate shape
  switch(_type) {
    case ShapeGenerator::cube:
      //shape = makeCube();
      break;
    case ShapeGenerator::sphere:
      //shape = makeCube(); // to change to sphere
      break;
    case ShapeGenerator::arrow:
      //shape = makeArrow();
      break;
    case ShapeGenerator::plane:
      shape = makePlane(_rows, _cols, _scale, _color);
      break;
    default:
      shape = makePlane(_rows, _cols, _scale, _color);
  }
  // increment shape count
  m_shapeCount++;

  // set shape name and add its name to the stored vector
  shape.m_name = _name;
  addShape(shape.name());

  // return new shape
  return shape;
}

//--------------------------ShapeGenerator::randomColor()---------------------------

glm::vec3 ShapeGenerator::randomColor()
{
  // random values between 0 and 1 (RAND_MAX is a constant defined in <cstdlib>)
  return glm::vec3(
        rand() / (float)RAND_MAX,
        rand() / (float)RAND_MAX,
        rand() / (float)RAND_MAX
        );
}
