#include <cloth.h>
#include <cmath>
#include <glm/geometric.hpp>

//-------------------------------Cloth::Cloth()-------------------------------------
Cloth::Cloth(std::string _name, ShapeData& _shape, float _stiffness, float _damping)
{
  // set cloth name
  m_name = _name + "_cloth";
  m_gravity = glm::vec3(0, -9.8f, 0);

  // store initial shape and shape data
  m_initialShape = _shape.data();
  m_numRows = _shape.numRows();
  m_numCols = _shape.numCols();

  // reserve space for all particles
  m_particles.resize(_shape.numVertices());

  // create particles
  glm::vec3 pos, vel;
  for(GLuint i = 0; i < _shape.numVertices(); i++)
  {
    pos = _shape.vertices()[i].position(); // get vert position from _shape

    m_particles[i] = new Particle(pos, vel); // create particle
    m_particles[i]->setId(m_idGen++); // set its unique id
  }

  // compute particle normals
  computeParticleNormals();

  // build springs
  buildSprings("structural");
  buildSprings("shear");
  buildSprings("bend");

  // set default stiffness(K) and damping on springs
  setStiffness(_stiffness);
  setDamping(_damping);

  // set default external force damping
  setExtDamping(_damping);

  // create integrator
  m_integrator.reset(new RK4(m_t, m_dt, _damping));
}

Cloth::~Cloth() {
  // delete all pointers stored on the mesh
  for(GLuint i = 0; i < m_particles.size(); i++) { delete m_particles[i]; }
  for(GLuint i = 0; i < m_springs.size(); i++) { delete m_springs[i]; }
}

//-------------------------------Cloth::rotate()-------------------------------------
void Cloth::rotate(glm::mat4 _matrix)
{
  for(Particle* particle : m_particles)
  {
    particle->setPosition(glm::vec3(_matrix * glm::vec4(particle->position(), 0)));
  }
}

//-------------------------------Cloth::reset()-------------------------------------
void Cloth::reset()
{
  // restore particle positions and velocity
  glm::vec3 zeroVec(0,0,0);
  for(Particle* particle : m_particles)
  {
    particle->setPosition( m_initialShape.vertices()[particle->id()].position() );
    particle->setVelocity(zeroVec);
    particle->setForce(zeroVec);
  }

  // recompute particle normals
  computeParticleNormals();

  // reset integrator
  setTime(0.0f);

  // reset internal timer
  setTime(0.0f);
}

//-------------------------------Cloth::shape()-------------------------------------
ShapeData Cloth::shape() const
{
  ShapeData data;

  // shallow copy simple type vars
  data.m_type = m_initialShape.type();
  data.m_name = m_initialShape.name();
  data.m_numVertices = m_initialShape.numVertices();
  data.m_numIndices = m_initialShape.numIndices();
  data.m_numRows = m_initialShape.numRows();
  data.m_numCols = m_initialShape.numCols();

  // deep copy the unique ptrs
  // vertices
  data.m_vertices.reset(new Vertex[data.m_numVertices]);
  for(GLuint i = 0; i < data.m_numVertices; i++)
  {
    data.m_vertices[i].setPosition(m_particles[i]->position());
    data.m_vertices[i].setColor(m_initialShape.m_vertices.get()[i].color());
    data.m_vertices[i].setNormal(m_particles[i]->normal());
  }

  // indices
  data.m_indices.reset(new GLushort[data.m_numIndices]);
  for(GLuint i = 0; i < data.m_numIndices; i++)
  {
    data.m_indices[i] = m_initialShape.m_indices[i];
  }

  // uvs
  data.m_uvs.reset(new GLfloat[data.m_numVertices * 2]);
  for(GLuint i = 0; i < m_initialShape.m_numVertices * 2; i+=2)
  {
    data.m_uvs[i] = m_initialShape.m_uvs[i]; // 0, 2, 4, 6 ...
    data.m_uvs[1+i] = m_initialShape.m_uvs[1+i]; // 1, 3, 5, 7 ...
  }

  return data;
}

//-------------------------------Cloth::buildSprings()------------------------------
void Cloth::buildSprings(std::string _type)
{

  // STRUCTURAL SPRINGS
  if(_type == "structural")
  {
    for(int i = 0; i < numParticles(); i++)
    {
      if(i%m_numCols < m_numCols - 1) // horizontal (_)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i+1], "structural", m_stiffness, m_damping));
      }
      if(i < m_numCols * (m_numRows - 1)) // vertical (|)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i+m_numCols], "structural", m_stiffness, m_damping));
      }
    }
  }

  // SHEARING SPRINGS
  else if(_type == "shear")
  {
    for(int i = 0; i < numParticles() - m_numCols; i++) // all minus last row
    {
      if(i%m_numRows < m_numRows - 1 && i%m_numCols < m_numCols - 1) // left-to-right (\)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i + m_numCols + 1], "shear", m_stiffness, m_damping));
      }
      if(i%m_numRows != 0 && i%m_numCols != 0) // right-to-left (/)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i + m_numCols - 1], "shear", m_stiffness, m_damping));
      }
    }
  }

  // BENDING SPRINGS
  else if(_type == "bend")
  {
    for(int i = 0; i < numParticles(); i++)
    {
      if(i%m_numCols < m_numCols - 2) // horizontal (__)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i + 2], "bend", m_stiffness, m_damping));
      }
      if(i < m_numCols * (m_numRows - 2)) // vertical (|)
      {
        m_springs.push_back(new Spring(*m_particles[i], *m_particles[i + (m_numCols * 2)], "bend", m_stiffness, m_damping));
      }
    }
  }

  // INVALID SPRINGS
  else
  {
    throw std::invalid_argument("Invalid spring type.");
  }
}

//------------------Cloth::computeParticleNormals()---------------------------------
void Cloth::computeParticleNormals()
{
  // TODO: simplify the method
  // create storage vectors
  glm::vec3 normal(0,0,0);
  glm::vec3 vec1(0,0,0);
  glm::vec3 vec2(0,0,0);

  // loop over all particles
  for(Particle* particle : m_particles)
  {
    // (1 TRI) particle on the UPPER RIGHT side of left-to-right diagonal (/*)
    if(particle->id() == m_numCols - 1)
    {
      vec1 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      vec2 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      normal = cross(vec1, vec2); // <- x |
    } // end (1 TRI)

    // (1 TRI) particle on the BOTTOM LEFT side of left-to-right diagonal (*/)
    else if(particle->id() == numParticles() - m_numCols)
    {
      vec1 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      vec2 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      normal = cross(vec1, vec2); // -> x |
    }

    // (2 TRIS) particle on the UPPER LEFT side of the left-to-right diagonal (*\)
    else if(particle->id() == 0)
    {
      vec1 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      vec2 = (m_particles[particle->id() + m_numCols + 1]->position() - particle->position()); // down + right
      normal += cross(vec1, vec2); // | x \ .

      vec1 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      normal += cross(vec2, vec1); // \ x ->
    }

    // (2 TRIS) particle on the BOTTOM RIGHT SIDE of the left-to-right diagonal (\*)
    else if(particle->id() == numParticles() - 1)
    {
      vec1 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      vec2 = (m_particles[particle->id() - m_numCols - 1]->position() - particle->position()); // up + left
      normal += cross(vec1, vec2); // | x \ .

      vec1 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      normal += cross(vec2, vec1); // \ x <-

    }

    // (3 TRIS) particles on the TOP edge (excluding corners)
    else if(particle->id() < m_numCols)
    {
      vec1 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      vec2 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      normal += cross(vec1, vec2); // <- x |

      vec1 = (m_particles[particle->id() + m_numCols + 1]->position() - particle->position()); // down + right
      normal +=cross(vec2, vec1); // | x \ .

      vec2 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      normal += cross(vec1, vec2); // \ x ->
    }

    // (3 TRIS) particles on the LEFT edge (excluding corners)
    else if(particle->id() % m_numCols == 0)
    {
      vec1 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      vec2 = (m_particles[particle->id() + m_numCols + 1]->position() - particle->position()); // down + right
      normal += cross(vec1, vec2); // | x \ .

      vec1 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      normal += cross(vec2, vec1); // \ x ->

      vec2 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      normal += cross(vec1, vec2); // -> x |
    }

    // (3 TRIS) particles on the BOTTOM edge (excluding corners)
    else if(particle->id() > m_numCols * (m_numRows-1) - 1)
    {
      vec1 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      vec2 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      normal += cross(vec1, vec2); // -> x |

      vec1 = (m_particles[particle->id() - m_numCols - 1]->position() - particle->position()); // up + left
      normal += cross(vec2, vec1); // | x \ .

      vec2 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      normal += cross(vec1, vec2); // \ x <-
    }

    // (3 TRIS) particles on the RIGHT edge (excluding corners)
    else if(particle->id() % m_numCols == m_numCols - 1)
    {
      vec1 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      vec2 = (m_particles[particle->id() - m_numCols - 1]->position() - particle->position()); // up + left
      normal += cross(vec1, vec2); // | x \ .

      vec1 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      normal += cross(vec2, vec1); // \ x <-

      vec2 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      normal += cross(vec1, vec2); // <- x |
    }

    // (6 TRIS) particles inside the mesh
    else
    {
      vec1 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      vec2 = (m_particles[particle->id() - m_numCols - 1]->position() - particle->position()); // up + left
      normal += cross(vec1, vec2); // | x \ .

      vec1 = (m_particles[particle->id() - 1]->position() - particle->position()); // left
      normal += cross(vec2, vec1); // \ x <-

      vec2 = (m_particles[particle->id() + m_numCols]->position() - particle->position()); // down
      normal += cross(vec1, vec2); // <- x |


      vec1 = (m_particles[particle->id() + m_numCols + 1]->position() - particle->position()); // down + right
      normal += cross(vec2, vec1); // | x \ .


      vec2 = (m_particles[particle->id() + 1]->position() - particle->position()); // right
      normal += cross(vec1, vec2); // \ x ->


      vec1 = (m_particles[particle->id() - m_numCols]->position() - particle->position()); // up
      normal += cross(vec2, vec1); // -> x \ .
    }

    // normalize result and store on particle
    if(normal != glm::vec3()) { normal = normalize(normal); }
    particle->setNormal(normal);
    //std::cout<<"Normal "<<particle->id()<<": "<<normal.m_x<<", "<<normal.m_y<<", "<<normal.m_z<<std::endl;
  } // end FOR

} // end computeParticleNormals()


//------------------Cloth::setStiffness()/Cloth::setDamping()-----------------------
void Cloth::setStiffness(float _stiffness)
{
  for(auto spring : m_springs) { spring->setK(_stiffness); }
}

void Cloth::setStiffness(int _springId, float _stiffness)
{
  m_springs[_springId]->setK(_stiffness);
}

void Cloth::setDamping(float _damping)
{
  for(auto spring : m_springs) { spring->setDamping(_damping); }
}

void Cloth::setDamping(int _springId, float _damping)
{
  m_springs[_springId]->setDamping(_damping);
}

//-------------------------------Cloth::update()------------------------------------
void Cloth::update(float _t, float _dt)
{
  // add internal -kx (and velocity damping -bv) force per particle
  for(Spring* spring : m_springs)
  {
    spring->storeForceOnParticles(spring->velocity());
  }

  // compute particle normals
  computeParticleNormals();

  // integrate particles
  for(Particle* particle : m_particles)
  {
    // add external forces
    if(!particle->isFixed()) {

      // gravity
      particle->addForce(m_gravity * _dt);

      // wind
      glm::vec3 n = particle->normal();
      glm::vec3 v = particle->velocity();
      glm::vec3 windDir = m_externalForces["wind"];
      glm::vec3 windVel(-std::sin(_t * m_windFreqMultiplier) * m_windAmp,
                        0.0f,
                        std::sin(_t * m_windFreqMultiplier) * m_windAmp);

      glm::vec3 windForce = windDir * std::abs(dot(n, windVel - v));
      particle->addForce(windForce * _dt);
    }

    // integrate
    m_integrator->integrate(*particle, _t, _dt);
  }
}

//-------------------------------getters--------------------------------------------
Particle* Cloth::particle(int _index)
{
  // NFO: gets pointer to specified particle
  // valid range checking
  if (_index >= 0 && _index < numParticles())
  {
    return m_particles[_index];
  }
  else { throw std::invalid_argument("Invalid particle index."); }
}

Spring* Cloth::spring(int _index)
{
  // NFO: gets pointer to specified particle
  // valid range checking
  if (_index >= 0 && _index < numSprings())
  {
    return m_springs[_index];
  }
  else { throw std::invalid_argument("Invalid spring index."); }
}

//---------------------------external force management------------------------------
void Cloth::addExtForce(std::string _name, const glm::vec3& _force)
{
  // store force name and vector on the cloth instance
  if(!isExtForce(_name)) { m_externalForces[_name] = _force; }
  else { std::cerr<<"An external force with the provided name already exists."<<std::endl; }
}

void Cloth::removeExtForce(std::string _name)
{
  // erase the force the stored external forces list
  if(isExtForce(_name)) { m_externalForces.erase(_name); }
  else { std::cerr<<"Invalid external force name."<<std::endl; }
}

bool Cloth::isExtForce(std::string _extF) const
{
  if(m_externalForces.find(_extF) != m_externalForces.end()) { return true; }
  else { return false; }
}

void Cloth::setExtForce(std::string _name, const glm::vec3& _force)
{
  if(isExtForce(_name)) { m_externalForces[_name] = _force; }
  else { std::cerr<<"Invalid external force name."<<std::endl; }
}

//---------------------------------utility------------------------------------------
void Cloth::setFixedSide(std::string _side, bool _isFixed)
{
  if(_side == "top")
  {
    for(GLuint i = 0; i < m_numCols; i++)
    {
      _isFixed ? particle(i)->setFixed(true) : particle(i)->setFixed(false);
    }
  }

  else if(_side == "left")
  {
    for(GLuint i = 0; i < numParticles(); i += m_numCols)
    {
      _isFixed ? particle(i)->setFixed(true) : particle(i)->setFixed(false);
    }
  }

  else if(_side == "bottom")
  {
    for(GLuint i = numParticles() - m_numCols; i < numParticles(); i++)
    {
      _isFixed ? particle(i)->setFixed(true) :particle(i)->setFixed(false);
    }
  }
  else if(_side == "right")
  {
    for(GLuint i = m_numCols - 1; i < numParticles(); i += m_numCols)
    {
      _isFixed ? particle(i)->setFixed(true) : particle(i)->setFixed(false);
    }
  }
  else if(_side == "none")
  {
    for(int i = 0; i < m_particles.size(); ++i)
    {
      particle(i)->setFixed(false);
    }
  }
  else
  {
    std::cerr<<"Invalid parameter specified. Valid parameters are: top, left, bottom, right"
             <<std::endl;
  }
}

void Cloth::debug()
{
  std::cout<<"***********Cloth::debug()************"<<std::endl;
  std::cout<<"Name: "<<name()<<std::endl;
  std::cout<<"Num Particles: "<<numParticles()<<std::endl;
  std::cout<<"Num Springs: "<<numSprings()<<std::endl;
  for(int i = 0; i < numParticles(); i++)
  {
    std::cout<<"Particle "<<i<<" pos: < "
             <<particle(i)->position().x<<", "
             <<particle(i)->position().y<<", "
             <<particle(i)->position().z
    <<" >"<<std::endl;
  }
  std::cout<<"***********/Cloth::debug()************"<<std::endl;
}
