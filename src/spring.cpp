#include "spring.h"
#include <glm/geometric.hpp>

//-------------------------------ctors/dtors------------------------------------------
Spring::Spring(Particle& _particleA, Particle& _particleB, std::string _type,
               float _k, float _damping)
{
  // set type
  m_type = _type;

  // set stiffness and damping
  m_k = _k;
  m_damping = _damping;

  // set spring length equal to the distance between particles
  m_restLength = glm::length(_particleB.position() - _particleA.position());

  // store pointers to particles connected to the spring
  m_particleA = &_particleA;
  m_particleB = &_particleB;
}

Spring::~Spring()
{
  if(m_particleA == nullptr) { delete m_particleA; }
  if(m_particleB == nullptr) { delete m_particleB; }
}

//-------------------------------update()---------------------------------------------
void Spring::update(const glm::vec3& _dxdt, const glm::vec3& _dvdt)
{
  //std::cout<<type()<<", "<<"<"<<pid()[0]<<", "<<pid()[1]<<">: "<<_dxdt.m_x<<", "<<_dvdt.m_x<<std::endl;
  // update the position and velocity of the particles
  if (!m_particleA->isFixed()) {
    //m_particleA->accumulate((-1)*_dxdt, (-1)*_dvdt);
  }
  if (!m_particleB->isFixed()) {
    //m_particleB->accumulate(_dxdt,_dvdt);
  }
}

//---------------------position / velocity--------------------------------------------
glm::vec3 Spring::position() const
{
  // position vector AB = B-A
  return m_particleB->position() - m_particleA->position();
}

glm::vec3 Spring::velocity() const
{
  // velocity vector AB = B-A
  return m_particleB->velocity() - m_particleA->velocity();
}

//-------------------------------Spring::displacement()-------------------------------
glm::vec3 Spring::displacement() const {
  // displacement vector AB = B-A
  glm::vec3 currVec = m_particleB->position() - m_particleA->position(); // vec from A to B
  glm::vec3 restVec = m_restLength * normalize(currVec); // rest vec
  return currVec - restVec; // difference vec (displacement)
}

//-------------------------------Spring::force()--------------------------------------
glm::vec3 Spring::force(const glm::vec3& _v) const
{
  // F = -kx -bv , where v = vA - vB
  glm::vec3 disp = this->displacement();
  glm::vec3 force = - m_k * disp - m_damping * _v;
  return force;
}

//-------------------------------Spring::storeForceOnParticles()----------------------
void Spring::storeForceOnParticles(const glm::vec3& _v)
{
  // F = -kx -bv , where v = vA - vB
  glm::vec3 disp = this->displacement();
  glm::vec3 force = - m_k * disp - m_damping * _v;

  m_particleA->addForce(-force);
  m_particleB->addForce(force);
}

//-------------------------------Spring::particleIds()--------------------------------
std::vector<int> Spring::particleIds() const
{
  std::vector<int> ids;
  ids.push_back(m_particleA->id());
  ids.push_back(m_particleB->id());
  return ids;
}
