#ifndef SHAPEDATA_H
#define SHAPEDATA_H

#include <vector>
#include <string>
#include <memory>

#include <vertex.h>

struct ShapeData // mesh generator output struct (might upgrade to class)
{

  ShapeData   data();
  GLsizeiptr  vertexBufferSize() const { return m_numVertices * sizeof(Vertex); }
  GLsizeiptr  indexBufferSize() const { return m_numIndices * sizeof(GLushort); }
  void        printVertices() const;
  std::string type() const { return m_type; }
  std::string name() const { return m_name; }
  const std::unique_ptr<Vertex[]>&   vertices() const { return m_vertices; }
  const std::unique_ptr<GLushort[]>& indices() const { return m_indices; }
  const std::unique_ptr<GLfloat[]>&  uvs() const { return m_uvs; }
  GLuint      numVertices() const { return m_numVertices; }
  GLuint      numIndices() const { return m_numIndices; }
  GLuint      numRows() const { return m_numRows; }
  GLuint      numCols() const { return m_numCols; }
  void        storeTo(std::vector<ShapeData*>& _toVec);
  void        clear();

  std::string m_type;
  std::string m_name;
  GLuint      m_numVertices;
  GLuint      m_numIndices;
  GLuint      m_numRows;
  GLuint      m_numCols;
  std::unique_ptr<Vertex[]> m_vertices;
  std::unique_ptr<GLushort[]> m_indices;
  std::unique_ptr<GLfloat[]> m_uvs;
};

#endif // SHAPEDATA_H

