#ifndef RK4_H
#define RK4_H

#include <particle.h>

//-------------------------------------------------------------------------
//---------class elements and general structure based on examples:---------
//---------http://gafferongames.com/game-physics/integration-basics/-------
//---------https://github.com/NCCA/NGL6Demos/tree/master/MassSpring--------

class Derivative
{
public:
  ~Derivative();
 glm::vec3 m_dx; // dx/dt (velocity)
 glm::vec3 m_dv; // dv/dt (acceleration)
};

class RK4
{
public:
  RK4(float _t, float _dt, float _gDamp) : m_t(_t), m_dt(_dt),
                                           m_globalDamping(_gDamp) {}

  // kinematics
  void       integrate(Particle& _particle, float _t, float _dt);
  Derivative evaluate(const Particle& _particle, float _t);
  Derivative evaluate(const Particle& _particle, float _t, float _dt,
                      const Derivative& _d);

  // time
  float      time() { return m_t; }
  void       setTime(float _t) { m_t = _t; }
  float      timestep() { return m_dt; }
  void       setTimestep(float _dt) { m_dt = _dt; }

private:
  RK4();
  float m_t;     // time
  float m_dt;    // timestep (delta t)
  float m_globalDamping; // global damping
};

#endif // RK4_H

