#ifndef CLOTH_H
#define CLOTH_H

#include <unordered_map>
#include <spring.h>
#include <shapeGenerator.h>
#include <glm/mat4x4.hpp>
#include <rk4.h>

class Cloth
{
public:
  // ctors & dtors
  Cloth(std::string _name, ShapeData& _shape, float _stiffness, float _damping);
  ~Cloth();

  // external force management (TODO: some arg error checking)
  const glm::vec3& gravity() const { return m_gravity; }
  bool  hasGravity() const { return m_gravityEnabled; }
  void  enableGravity() { m_gravityEnabled = true; }
  void  setGravity(glm::vec3 _gravity) { m_gravity = _gravity; }
  void  removeGravity() { m_gravityEnabled = false; }
  const glm::vec3& extForce(std::string _name) { return m_externalForces[_name]; }
  bool  isExtForce(std::string _extF) const;
  void  setExtForce(std::string _name, const glm::vec3& _force);
  void  addExtForce(std::string _name, const glm::vec3& _force);
  void  removeExtForce(std::string _name);
  float extDamping() const { return m_extDamping; }
  void  setExtDamping(float _damping) { m_extDamping = _damping; } // external force damping
  void  setWindAmp(float _amp) { m_windAmp = _amp; }
  void  setWindFreqMultiplier(float _mult) { m_windFreqMultiplier = _mult; }
  void  computeParticleNormals();

  // internal force management
  void buildSprings(std::string _type);
  void setStiffness(float _stiffness); // all springs
  void setStiffness(int _springId, float _stiffness); // one spring
  void setDamping(float _damping); // all springs
  void setDamping(int _springId, float _damping); // one spring

  // integrator management
  float time() const { return m_t; }
  void  setTime(float _time) { m_integrator->setTime(_time); m_t = _time; }
  float timestep() const { return m_dt; }
  void  setTimestep(float _timestep) { m_integrator->setTimestep(_timestep); m_dt = _timestep; }

  // getters
  Particle* particle(int _index);
  Spring*   spring(int _index);
  ShapeData shape() const;

  // utility
  std::string name() const { return m_name; }
  int         numParticles() const { return m_particles.size(); }
  int         numSprings() const { return m_springs.size(); }
  int         numRows() const { return m_numRows; }
  int         numCols() const { return m_numCols; }
  void        setFixedSide(std::string _side, bool _isFixed = true);
  void        debug();
  void        rotate(glm::mat4 _matrix);

  // main update/reset
  void update(float _t, float _dt);
  void reset();

private:
  // cloth info
  Cloth();
  std::string m_name;

  // shape info
  ShapeData m_initialShape; // initial shape that was passed in the constructor
  int       m_numRows; // number of particle rows
  int       m_numCols; // number of particle columns

  // integrator
  std::unique_ptr<RK4> m_integrator;
  float m_t;
  float m_dt;

  // particles, springs
  std::vector<Particle*> m_particles; // pointers to all particles of the mesh
  std::vector<Spring*>   m_springs; // pointers to all springs of the mesh
  float m_stiffness;
  float m_damping;
  float m_extDamping;

  // external forces
  glm::vec3 m_gravity;
  bool      m_gravityEnabled = false;
  float     m_windAmp = 5.0f;
  float     m_windFreqMultiplier = 0.5f;
  std::unordered_map<std::string, glm::vec3> m_externalForces; // stores external forces by name

  // particle id generator
  int m_idGen = 0;
};


#endif // CLOTH_H

