#ifndef SPRING_H
#define SPRING_H

#include <particle.h>
#include <glm/geometric.hpp>
#include <vector>
#include <memory>

//---------class elements and general structure loosely based on examples:----
//---------https://github.com/NCCA/NGL6Demos/tree/master/MassSpring-----------

class Spring
{
public:
  // ctors & dtors
  Spring(Particle& _particleA, Particle& _particleB, std::string _type,
         float _k, float _damping);
  ~Spring();

  // updates the position and velocity of the particles
  void update(const glm::vec3& _dxdt, const glm::vec3& _dvdt);

  // (particle) position & velocity
  Particle* particleA() { return m_particleA; }
  Particle* particleB() { return m_particleB; }
  const glm::vec3& positionA() const { return m_particleA->position(); }
  const glm::vec3& positionB() const { return m_particleB->position(); }
  const glm::vec3& velocityA() const { return m_particleA->velocity(); }
  const glm::vec3& velocityB() const { return m_particleB->velocity(); }

  // (spring) position & velocity
  glm::vec3 position() const; // Spring.pos (AB) = B.pos - A.pos
  glm::vec3 velocity() const; // Spring.vel (AB) = B.vel - A.vel

  // forces
  float     length() const { return glm::length(m_particleB->position() - m_particleA->position()); }
  glm::vec3 displacement() const; //(difference between rest vec and current vec)
  glm::vec3 force(const glm::vec3& _v) const; //returns the Force of the spring (F = -kx -bv)
  void      storeForceOnParticles(const glm::vec3& _v);

  // parameter management
  std::string type() const { return m_type; }
  void  setType(std::string _type) { m_type = _type; }

  float restLength() const { return m_restLength; }
  void  setLength(float _length) { m_restLength = _length; }

  float k() const { return m_k; }
  void  setK(float _k) { m_k = _k; }

  float damping() const { return m_damping; }
  void  setDamping(float _damping) { m_damping = _damping; }

  // utils
  std::vector<int> particleIds() const; // return connected particle ids

private:
  std::string m_type; // type of spring

  float m_k; // spring hardness (Hooke's law: F = -kX)
  float m_damping; // damper value for the spring (F = -kX -bv, b=m_damping)
  float m_restLength; // spring's rest length

  Particle* m_particleA = nullptr;
  Particle* m_particleB = nullptr;
};

#endif // SPRING_H

