#ifndef PARTICLE_H
#define PARTICLE_H

#include <glm/vec3.hpp>
#include <iostream>
#include <utils.h>

class Particle
{
public:
  // ctors & dtors
  Particle();
  Particle(const glm::vec3& _position, const glm::vec3& _velocity);

  // kinematics
  const glm::vec3&  position() const { return m_position; }
  void       setPosition(const glm::vec3& _position) { m_position = _position; }
  const glm::vec3&  velocity() const { return m_velocity; }
  void       setVelocity(const glm::vec3& _velocity) { m_velocity = _velocity; }
  void       advanceState(const glm::vec3& _x, const glm::vec3& _v);
  void       setState(const glm::vec3& _x, const glm::vec3& _v);
  void       update(const glm::vec3& _dxdt, const glm::vec3& _dvdt);

  const glm::vec3& force() const { return m_force; }
  void       addForce(const glm::vec3& _f) { m_force += _f; }
  void       setForce(const glm::vec3& _f) { m_force  = _f; }

  // utility
  const glm::vec3& normal() const { return m_normal; }
  void       setNormal(const glm::vec3& _normal) { m_normal = _normal; }
  bool       isFixed() const { return m_fixed; }
  void       setFixed(bool _fix) { m_fixed = _fix; }

  // debug
  int        id() const { return m_id; }
  void       setId(int _id) { m_id = _id; }
private:
  glm::vec3  m_position;
  glm::vec3  m_velocity;
  glm::vec3  m_force;
  glm::vec3  m_normal;
  bool       m_fixed = false;
  bool       m_gravityEnabled = false;
  float      m_mass = 1.0f; // attr. not used in practice. unit mass assumed for all p.
  int        m_id;
};

#endif // PARTICLE_H

