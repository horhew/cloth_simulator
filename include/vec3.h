#ifndef VEC3_H
#define VEC3_H

#include <iostream>
#include <math.h>

class Vec3
{
public:
  Vec3(float _x = 0, float _y = 0, float _z = 0) :x(_x), y(_y), z(_z) {
    m_length = sqrt(x*x + y*y + z*z);
  }

  // operator overloads
  Vec3 operator  +(const Vec3& _v) const;
  Vec3 operator  -(const Vec3& _v) const;
  void operator +=(const Vec3& _v);
  void operator -=(const Vec3& _v);
  float operator *(const Vec3& _v) const; // equivalent to dot()
  Vec3 operator *(float _multiplier) const; // post-multiplication
  friend Vec3 operator *(float _f, const Vec3& _v); // pre-multiplication
  Vec3 operator  /(float _divisor) const;
  bool operator ==(const Vec3& _v) const;
  bool operator !=(const Vec3& _v) const;
  friend std::ostream& operator<<(std::ostream& _output, const Vec3& _v);

  // normalization
  Vec3  normal() const; // return normalized vector
  void  normalize(); // in-place normalization

  // dot & cross products
  float dot(const Vec3& _v) const;
  Vec3  cross(const Vec3& _v, bool _normalize = false) const;

  // utilities
  float length() const;
  void  print() const;
  bool  compare(float _a, float _b) const;

  float x, y, z;
private:
  float m_length;
};

#endif // VEC3

