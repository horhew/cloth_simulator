#ifndef SHAPEGENERATOR_H
#define SHAPEGENERATOR_H

#include <glm/vec3.hpp>
#include <unordered_map>
#include <shapeData.h>

class ShapeGenerator // mesh generator class
{
public:
  enum ShapeType {cube, sphere, arrow, plane};
  ShapeData make(std::string _name,
                 ShapeType _type = ShapeType::plane,
                 glm::vec3 _color = glm::vec3(1,0,0),
                 uint _rows = 10,
                 uint _cols = 10,
                 float _scale = 1);

  int  shapeCount() { return m_shapeCount; }
  void clearShapes() { m_shapeNames.clear(); m_shapeCount = 0; }
  std::vector<std::string> shapeNames() { return m_shapeNames; }

private:
  // make specific geometry
  ShapeData makeTriangle();
  ShapeData makeCube();
  ShapeData makeArrow();
  ShapeData makePlaneVerts(uint _rows, uint _cols, float _scale, glm::vec3 _color);
  ShapeData makePlaneIndices(uint _rows, uint _cols);
  ShapeData makePlane(uint _rows, uint _cols, float _scale, glm::vec3 _color);

  // utils
  void addShape(std::string _shapeName) { m_shapeNames.push_back(_shapeName); }
  glm::vec3 randomColor();

  int m_shapeCount = 0; // keeps track how many pieces of geometry were created
  std::vector<std::string> m_shapeNames; // keeps record of all created geometry names
};


#endif // SHAPEGENERATOR_H
