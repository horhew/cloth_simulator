#ifndef VERTEX_H
#define VERTEX_H

#include <glm/vec3.hpp>
#include <ngl/Types.h>

class Vertex // Vertex (position, color, normal) class
{
public:
  Vertex();
  Vertex(glm::vec3 _position, glm::vec3 _color, glm::vec3 _normal) :
                m_position(_position),
                m_color(_color),
                m_normal(_normal) {}
  Vertex(float _x, float _y, float _z,
         float _r, float _g, float _b,
         float _nx, float _ny, float _nz) :
         m_position(glm::vec3(_x, _y, _z)),
         m_color(glm::vec3(_r, _g, _b)),
         m_normal(glm::vec3(_nx, _ny, _nz)) {}
  ~Vertex();
  // getters and setters
  const glm::vec3& position() const { return m_position; }
  const glm::vec3& color() const { return m_color; }
  const glm::vec3& normal() const { return m_normal; }
  void setPosition(const glm::vec3& _position) { m_position = _position; }
  void setColor(const glm::vec3& _color) { m_color = _color; }
  void setNormal(const glm::vec3& _normal) { m_normal = _normal; }
private:
  glm::vec3 m_position;
  glm::vec3 m_color;
  glm::vec3 m_normal;
};

#endif // VERTEX_H

