#ifndef NGLSCENE_H__
#define NGLSCENE_H__
#include <ngl/Types.h>
#include <QOpenGLWindow>
#include <QGLWidget>
#include <iostream>
#include <unordered_map>
#include <memory>
#include <camera.h>
#include <shapeGenerator.h>
#include <cloth.h>
//----------------------------------------------------------------------------------------------------------------------
//Various classes and elements throughout the program based on examples:

//http://gafferongames.com/game-physics/integration-basics/
//https://github.com/NCCA/NGL6Demos/tree/master/MassSpring
// https://www.youtube.com/watch?v=6c1QYZAEP2M&list=PLRwVmtr-pp06qT6ckboaOhnm9FxmzHpbY
//----------------------------------------------------------------------------------------------------------------------
class NGLScene : public QOpenGLWindow
{
public:
  NGLScene();
  ~NGLScene();

  void initializeGL(); // called just after OpenGL context is created
  void paintGL();
  void resizeGL(QResizeEvent* _event); // qt 5.5
  void resizeGL(int _w, int _h); // qt 5.4
  void test();

  //void createLight(std::string _type, );

  // data creation
  void createSharedVbo(const std::vector<ShapeData*>& _shapes);
  void storeNumIndices(const std::vector<ShapeData*>& _shapes);
  void createVaos(const std::vector<ShapeData*>& _shapes);
  void installShaders();

  // data handling
  void drawShape(const std::string _shapeName, // shape name
                 const glm::mat4& _mM, // model matrix
                 const glm::mat4& _vpM, // view-projection matrix
                 const GLenum _drawMethod = GL_TRIANGLES); // draw method

  // utils
  void bgColor(int _r, int _g, int _b, float _a = 1.0f);
  void resetAllCloths();
  void exportObj(std::string _filename);

private:
  // event methods
  void keyPressEvent(QKeyEvent* _event);
  void keyReleaseEvent(QKeyEvent* _event);
  void mouseMoveEvent (QMouseEvent* _event );
  void mousePressEvent ( QMouseEvent* _event);
  void mouseReleaseEvent ( QMouseEvent* _event );
  void wheelEvent( QWheelEvent* _event);
  void timerEvent(QTimerEvent*);

  // shader-related methods
  std::string readShaderCode(const char* _filename);
  void        checkStatus(const GLuint& _objID, std::string _objName = "Object",
                          bool _isShader = true);

  // program id
  GLuint m_programID;

  // camera
  Camera m_camera;
  bool m_clicked = false;

  // cloth objects & variables
  std::unordered_map<std::string, std::unique_ptr<Cloth>> m_clothObjects;
  std::unordered_map<std::string, std::vector<glm::mat4>> m_clothRotationMatrices;
  float m_windFreqMultiplier;
  float m_windAmp;
  bool m_fixedTop = false;
  bool m_fixedRight = false;
  bool m_fixedBottom = false;
  bool m_fixedLeft = false;

  // shape info
  ShapeGenerator m_shapeGen;
  std::unordered_map<std::string, ShapeData> m_shapes; // shapes
  std::unordered_map<std::string, GLuint> m_numIndices; // num indices per shape

  // VBOs,VAOs,VSO,FSO id storage
  GLuint m_vbo; // shared vertex buffer id
  std::unordered_map<std::string, GLuint> m_vaos; // vertex array ids
  std::unordered_map<std::string, GLuint> m_indexOffset; // index data offsets (VAOs)
  GLuint m_vso; // vertex shader id
  GLuint m_fso; // fragment shader id
  std::unordered_map<std::string, GLint> m_uniformLocations; // shader uniform locations

  // cloth sim time
  float m_time = 0.0f;
  float m_dtime = 0.05f;
  int   m_timer;
};

#endif
