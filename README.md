## Cloth Simulator

A basic OpenGL cloth simulator written in C++ as a uni assignment. It has no fancy features at the moment, but I am planning to add in a few of them (e.g. spring length correction, collisions, tearing) as soon as time permits. It is a straight implementation of Xavier Provot's 1995 paper, _"Deformation Constraints in a Mass-Spring Model to Describe Rigid Cloth Behavior"_.

This was a really good project to get my feet wet with physically-based simulation and making it work was a very satisfying experience in the end, but I have to admit that not having done this sort of thing before, actually getting there was not easy. So if, by any chance, whoever is reading this struggles with writing a cloth simulator themselves, know that I struggled too.


***

### External libraries

ngl, glm

### Hotkeys

**_Camera:_**

WASD, R,F, mouse : move and pan camera

C : reset camera

T, G : openGL drawing mode (GL_LINE, GL_FILL)

**_Cloth:_**

1-9 : wind strength

Q, E : increase/decrease wind frequency

UP, DOWN: increase/decrease wind amplitude

Home, PgUp, PgDown, End : toggle constrain for top/right/bottom/left sides of the cloth

Ins : unconstrain all sides

X : reset all cloths

M : export cloth as an OBJ
